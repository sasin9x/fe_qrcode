import 'dart:convert';
import 'package:FE/screen/auth/payment.dart';
import 'package:FE/screen/auth/scan_payment.dart';
import 'package:FE/screen/auth/payment_history.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:FE/api/api.dart';
import 'package:FE/screen/auth/information.dart';
import 'package:FE/screen/login.dart';
import 'package:FE/screen/auth/create_qrcode.dart';

void main() => runApp(HomePage());

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Thanh toán điện tử'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications_none_rounded),
              onPressed: null,
            ),
            IconButton(
              icon: Icon(Icons.account_circle_outlined),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => InformationPage())),
            ),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Thanh toán điện tử',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                        ),
                      ),
                    ],
                  )),
              ListTile(
                leading:
                    Icon(Icons.attach_money_outlined, color: Colors.blueAccent),
                title: Text('Nạp tiền',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blueAccent,
                    )),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.cached_outlined, color: Colors.blueAccent),
                title: Text('Chuyển tiền',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blueAccent,
                    )),
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentPage())),
              ),
              ListTile(
                leading: Icon(Icons.qr_code_outlined, color: Colors.blueAccent),
                title: Text('Tạo mã QR',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blueAccent,
                    )),
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CreateQRCodePage())),
              ),
              ListTile(
                leading: Icon(Icons.qr_code_scanner_outlined,
                    color: Colors.blueAccent),
                title: Text('QR Pay',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blueAccent,
                    )),
                onTap: hanldeScanQR,
              ),
              ListTile(
                leading: Icon(Icons.history_rounded, color: Colors.blueAccent),
                title: Text('Lịch sử giao dịch',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blueAccent,
                    )),
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PaymentHistoryPage())),
              ),
              ListTile(
                leading: Icon(Icons.logout, color: Colors.blueAccent),
                title: Text('Đăng xuất',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blueAccent,
                    )),
                onTap: hanldeLogout,
              ),
            ],
          ),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Image.network(
                  'https://media.istockphoto.com/vectors/qr-code-scan-phone-icon-in-comic-style-scanner-in-smartphone-vector-vector-id1166145556'),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: Row(children: <Widget>[
                  Column(children: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PaymentPage())),
                      child: Icon(
                        Icons.cached_outlined,
                        size: 60,
                        color: Colors.blueAccent,
                      ),
                    ),
                    Text(
                      "Chuyển tiền",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blueAccent,
                      ),
                    ),
                  ]),
                  Column(children: <Widget>[
                    FlatButton(
                      onPressed: null,
                      child: Icon(
                        Icons.attach_money_outlined,
                        size: 60,
                        color: Colors.blueAccent,
                      ),
                    ),
                    Text(
                      "Nạp tiền",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blueAccent,
                      ),
                    ),
                  ]),
                  Column(children: <Widget>[
                    FlatButton(
                      onPressed: hanldeScanQR,
                      child: Icon(
                        Icons.qr_code_scanner_outlined,
                        size: 60,
                        color: Colors.blueAccent,
                      ),
                    ),
                    Text(
                      "QR Pay",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blueAccent,
                      ),
                    ),
                  ]),
                ]),
              ),
            ],
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

  void handleMenu() {}

  void hanldeLogout() async {
    var res = await CallAPI().getData('logout');
    var body = json.decode(res.body);
    if (body['status']) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.remove('token');

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
    }
  }

  void hanldeScanQR() async {
    String scaning = await BarcodeScanner.scan();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ScanPaymentPage(resultScan: scaning)));
  }
}
