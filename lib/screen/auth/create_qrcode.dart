import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:FE/api/api.dart';
// import 'package:vietnamese_number_reader/vietnamese_number_reader.dart';

void main() => runApp(CreateQRCodePage());

class CreateQRCodePage extends StatefulWidget {
  @override
  _CreateQRCodePageState createState() => new _CreateQRCodePageState();
}

class _CreateQRCodePageState extends State<CreateQRCodePage> {
  String qrData = '';
  bool _show = false;
  var userData;
  TextEditingController _moneyController = new TextEditingController();

  @override
  void initState() {
    _getUser();
    super.initState();
  }

  void _getUser() async {
    var res = await CallAPI().getData('account');
    var body = json.decode(res.body);

    setState(() {
      userData = body['data'];
    });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false)),
          title: Text('Tạo mã QR'),
        ),
        body: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(50, 50, 50, 0),
            child: TextField(
              controller: _moneyController,
              keyboardType: TextInputType.number,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
              decoration: InputDecoration(
                  labelText: "Số tiền",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder()),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(100, 10, 100, 0),
            child: Column(children: <Widget>[
              FlatButton(
                child: Text(
                  'Tạo',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 18,
                  ),
                ),
                onPressed: handleCreateQR,
              ),
              _show ? QrImage(data: qrData) : Text(''),
            ]),
          ),
        ]),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

  void handleCreateQR() {
    setState(() {
      _show = true;
      qrData =
          '${userData['id']}|${userData['name']}|${userData['phone']}|${userData['email']}|${_moneyController.text}';
    });
  }
}
