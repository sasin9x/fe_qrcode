import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:FE/api/api.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

void main() => runApp(PaymentPage());

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  bool _isErrorMoney = false;
  TextEditingController _nameSender,
      _yourBalance,
      _idReciver,
      _phoneReciver,
      _nameReciver,
      _money,
      _content;
  var userData, _errorMoney;

  void initState() {
    _getUser();
    super.initState();
  }

  void _getUser() async {
    var res = await CallAPI().getData('account');
    var body = json.decode(res.body);

    setState(() {
      userData = body['data'];
    });

    _nameSender = new TextEditingController(
        text: userData != null ? userData['name'] : '');
    _yourBalance = new TextEditingController(
        text: userData != null
            ? '${FlutterMoneyFormatter(amount: userData['account_balance']['money'].toDouble()).output.withoutFractionDigits} đ'
            : '');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false)),
          title: Text('Thanh toán điện tử'),
        ),
        body: SingleChildScrollView(
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Người gửi',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  TextField(
                    controller: _nameSender,
                    enabled: false,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xffc0c0c0),
                        labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                        border: OutlineInputBorder()),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Số dư tài khoản',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  TextField(
                    controller: _yourBalance,
                    enabled: false,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xffc0c0c0),
                        labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                        border: OutlineInputBorder()),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Số điện thoại người nhận',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  TextField(
                    controller: _phoneReciver,
                    keyboardType: TextInputType.number,
                    onChanged: hanldeGetNameReciver,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                        border: OutlineInputBorder()),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Tên người nhận',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  TextField(
                    controller: _nameReciver,
                    enabled: false,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                        border: OutlineInputBorder()),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Số tiền',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  TextField(
                    controller: _money,
                    keyboardType: TextInputType.number,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    onChanged: handleChangeMoney,
                    decoration: InputDecoration(
                        labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                        border: OutlineInputBorder(),
                        errorText: _isErrorMoney ? _errorMoney : null),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      'Nội dung',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  TextField(
                    controller: _content,
                    maxLines: 5,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                    decoration: InputDecoration(
                        labelStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                        border: OutlineInputBorder()),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 150),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  color: Colors.greenAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(
                    'Thanh toán',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: handlePayment,
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  void hanldeGetNameReciver(phone) async {
    Pattern pattern = r'^(?:[+0]9)?[0-9]{10}$';
    RegExp regex = new RegExp(pattern);
    int i = 0;
    if (!regex.hasMatch(phone)) {
      //Khong dung dinh dang số điện thoại
    } else {
      var data = {
        'phone': phone,
      };

      var res = await CallAPI().postData(data, 'account-by-phone');
      var body = json.decode(res.body);

      if (body['data'] != null) {
        setState(() {
          _idReciver =
              new TextEditingController(text: body['data']['id'].toString());
          _nameReciver = new TextEditingController(text: body['data']['name']);
        });
      } else {
        //Thông báo không tồn tại người dùng
      }
    }
  }

  void handleChangeMoney(money) {
    int _money = int.parse(money);

    if (_money < 0 || _money > userData['account_balance']['money']) {
      setState(() {
        _isErrorMoney = true;
        _errorMoney = 'Không đủ chi phí để thanh toán';
      });
    } else {
      setState(() {
        _isErrorMoney = false;
      });
    }
  }

  void handlePayment() async {
    // if (!_isErrorMoney) {
    //   //thanh toán khi không có 1 lỗi gì xảy ra (VD: số tiền đủ để thanh toán)

    //   var data = {
    //     'reciverId': _idReciver.text,
    //     'phone': _phoneReciver.text,
    //     'money': _money.text,
    //     'content': _content.text,
    //   };

    //   var res = await CallAPI().postData(data, 'payment');
    //   var body = json.decode(res.body);
    // }
    print(_idReciver.text);
    print(_nameReciver.text);
    print(_money);
    print(_content);
  }
}
