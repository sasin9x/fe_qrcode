import 'dart:convert';

import 'package:FE/api/api.dart';
import 'package:FE/screen/auth/confirmPayment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

void main() => runApp(ScanPaymentPage());

class ScanPaymentPage extends StatefulWidget {
  String resultScan;
  ScanPaymentPage({this.resultScan});

  @override
  _ScanPaymentPageState createState() => new _ScanPaymentPageState(resultScan);
}

class _ScanPaymentPageState extends State<ScanPaymentPage> {
  String resultScan;
  _ScanPaymentPageState(this.resultScan);

  bool _isErrorMoney = false;
  TextEditingController _nameSender,
      _yourBalance,
      _idReciver,
      _phoneReciver,
      _emailReciver,
      _nameReciver,
      _money,
      _content;
  var userData, _errorMoney;

  initState() {
    _getUser();
    var result = resultScan.split('|');
    super.initState();
    _idReciver = new TextEditingController(text: result[0]);
    _nameReciver = new TextEditingController(text: result[1]);
    _phoneReciver = new TextEditingController(text: result[2]);
    _emailReciver = new TextEditingController(text: result[3]);
    _money = new TextEditingController(text: result[4]);
    _content = new TextEditingController();
  }

  void _getUser() async {
    var res = await CallAPI().getData('account');
    var body = json.decode(res.body);

    setState(() {
      userData = body['data'];
    });

    _nameSender = new TextEditingController(
        text: userData != null ? userData['name'] : '');
    _yourBalance = new TextEditingController(
        text: userData != null
            ? '${FlutterMoneyFormatter(amount: userData['account_balance']['money'].toDouble()).output.withoutFractionDigits} đ'
            : '');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text('Trang thanh toán'),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false)),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Người gửi',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                      controller: _nameSender,
                      enabled: false,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xffc0c0c0),
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder()),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Số dư tài khoản',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                      controller: _yourBalance,
                      enabled: false,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xffc0c0c0),
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder()),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Số điện thoại người nhận',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                      controller: _phoneReciver,
                      enabled: false,
                      keyboardType: TextInputType.number,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xffc0c0c0),
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder()),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Người nhận',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                      controller: _nameReciver,
                      enabled: false,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xffc0c0c0),
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder()),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Số tiền',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                      controller: _money,
                      keyboardType: TextInputType.number,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      onChanged: handleChangeMoney,
                      decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder(),
                          errorText: _isErrorMoney ? _errorMoney : ''),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 5, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Nội dung',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                      controller: _content,
                      maxLines: 5,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder()),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 20, 50, 150),
                child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.greenAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Text(
                      'Thanh toán',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    onPressed: _money.text.length > 0  ? handlePayment : null,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

  void handlePayment() async {
    if (!_isErrorMoney) {
      //thanh toán khi không có 1 lỗi gì xảy ra (VD: số tiền đủ để thanh toán)

      var data = {
        'reciverId': _idReciver.text,
        'phone': _phoneReciver.text,
        'email': _emailReciver.text,
        'money': _money.text,
        'content': _content.text,
      };

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ConfirmPaymentPage(
                    dataPayment: data,
                  )));
    }
  }

  void handleChangeMoney(money) {
    int _money = int.parse(money);

    if (_money < 0 || _money > userData['account_balance']['money']) {
      setState(() {
        _isErrorMoney = true;
        _errorMoney = 'Không đủ chi phí để thanh toán';
      });
    } else {
      setState(() {
        _isErrorMoney = false;
      });
    }
  }
}
