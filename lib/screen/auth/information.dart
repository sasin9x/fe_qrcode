import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:FE/api/api.dart';

void main() => runApp(InformationPage());

class InformationPage extends StatefulWidget {
  @override
  _InformationPageState createState() => new _InformationPageState();
}

class _InformationPageState extends State<InformationPage> {
  String qrData = '';
  bool _show = false;
  var userData;
  @override
  
  void initState() {
    _getUser();
    super.initState();
  }

  void _getUser() async {
    var res = await CallAPI().getData('account');
    var body = json.decode(res.body);

    setState(() {
      userData = body['data'];
    });
  }
  
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false)),
          title: Text('Thông tin người dùng'),
        ),
        body: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(50, 20, 50, 10),
            child: Column(children: <Widget>[
              Text(
                userData != null ? 'Số dư: ${FlutterMoneyFormatter( amount: userData['account_balance']['money'].toDouble()).output.withoutFractionDigits}đ' : '',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.blueAccent,
                ),
              ),
            ]),
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.account_circle_outlined, 
                color: Colors.blue,
              ),
              title: Text(
                userData != null ? userData['name'] : '',
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.mail_outline_rounded, 
                color: Colors.blue,
              ),
              title: Text(
                userData != null ? userData['email'] : '',
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.local_phone_rounded, 
                color: Colors.blue,
              ),
              title: Text(
                userData != null ? userData['phone'] : '',
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Card(
            child: ListTile(
              leading: Icon(
                Icons.location_on_outlined, 
                color: Colors.blue,
              ),
              title: Text(
                userData != null ? userData['address'] : '',
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(100, 10, 100, 0),
            child: Column(children: <Widget>[
              FlatButton(
                child: Text(
                  'Mã QR của tôi',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 18,
                  ),
                ),
                onPressed: handleCreateQR,
              ),
              _show ? QrImage(data: qrData) : Text(''),
            ]),
          ),
        ]),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

  void handleCreateQR(){
    setState(() {
      _show = true;
      qrData = '${userData['id']}|${userData['name']}|${userData['phone']}|${userData['email']}|';
    });
  }
}
