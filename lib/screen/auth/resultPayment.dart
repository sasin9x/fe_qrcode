import 'dart:convert';

import 'package:FE/api/api.dart';
import 'package:FE/screen/auth/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

void main() => runApp(ResultPaymentPage());

class ResultPaymentPage extends StatefulWidget {
  var dataPayment;
  ResultPaymentPage({this.dataPayment});

  @override
  _ResultPaymentPageState createState() =>
      new _ResultPaymentPageState(dataPayment);
}

class _ResultPaymentPageState extends State<ResultPaymentPage> {
  var dataPayment;
  _ResultPaymentPageState(this.dataPayment);

  initState() {
    super.initState();
    print(dataPayment);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Kết quả giao dịch'),
          leading: IconButton(
              icon: Icon(Icons.home),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomePage()))),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
                child: Icon(
                  Icons.check_circle,
                  color: Colors.greenAccent,
                  size: 69,
                ),
              ),
              Text(
                'Thanh toán thành công',
                style: TextStyle(
                  // color: Colors.greenAccent,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '${FlutterMoneyFormatter(amount: int.parse(dataPayment['money']).toDouble()).output.withoutFractionDigits}đ',
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Divider(
                height: 40,
                thickness: 1,
                color: Colors.blueAccent,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 20, 50, 150),
                child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.blueAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Text(
                      'Trở về màn hình chính',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage())),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
