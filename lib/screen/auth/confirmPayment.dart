import 'dart:convert';

import 'package:FE/api/api.dart';
import 'package:FE/screen/auth/resultPayment.dart';
import 'package:flutter/material.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

void main() => runApp(ConfirmPaymentPage());

class ConfirmPaymentPage extends StatefulWidget {
  var dataPayment;
  ConfirmPaymentPage({this.dataPayment});

  @override
  _ConfirmPaymentPageState createState() =>
      new _ConfirmPaymentPageState(dataPayment);
}

class _ConfirmPaymentPageState extends State<ConfirmPaymentPage> {
  var dataPayment;
  _ConfirmPaymentPageState(this.dataPayment);
  bool isLoading = true;
  TextEditingController _otp = new TextEditingController();

  Duration _duration = Duration(minutes: 1);

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showSnackBar(mess, color) {
    final snackBar = new SnackBar(
      content: Text(mess),
      duration: Duration(seconds: 3),
      backgroundColor: color,
      width: 280.0,
      padding: EdgeInsets.symmetric(horizontal: 10),
      behavior: SnackBarBehavior.floating,
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Xác nhận thanh toán'),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false)),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Nhập mã OTP',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    TextField(
                        controller: _otp,
                        keyboardType: TextInputType.number,
                        textCapitalization: TextCapitalization.words,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        ),
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                          border: OutlineInputBorder(),
                        )),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: FlatButton(
                    child: Text(
                      'Nhận mã OTP',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 18,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    onPressed: isLoading ? hanldeCreateOTP : null,
                  ),
                ),
              ),
              isLoading
                  ? Text('')
                  : Padding(
                      padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                      child: SlideCountdownClock(
                        duration: _duration,
                        slideDirection: SlideDirection.Down,
                        separator: ':',
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        onDone: hanldeDone,
                      ),
                    ),
              Padding(
                padding: EdgeInsets.fromLTRB(50, 20, 50, 150),
                child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.greenAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Text(
                      'Xác nhận',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    onPressed: isLoading ? null : hanldeConfirm,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

  void hanldeDone() {
    setState(() {
      isLoading = true;
    });
  }

  void hanldeCreateOTP() async {
    var data = {
      'email': dataPayment['email'],
    };
    var res = await CallAPI().postData(data, 'sent-OTP');
    var body = json.decode(res.body);
    print(body);
    if (body['status'] == true) {
      setState(() {
        isLoading = false;
      });
    }
  }

  void hanldeConfirm() async {
    var resOTP = await CallAPI().postData({'otp': _otp.text}, 'check-OTP');
    var bodyResOTP = json.decode(resOTP.body);
    if (bodyResOTP['status'] == true) {
      var res = await CallAPI().postData(dataPayment, 'payment');
      var body = json.decode(res.body);
      if (body['status'] == true) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ResultPaymentPage(dataPayment: dataPayment,)));
      }
      else{
        _showSnackBar(bodyResOTP['message'], Colors.redAccent);
      }
    } else {
      _showSnackBar(bodyResOTP['message'], Colors.redAccent);
    }
  }
}
