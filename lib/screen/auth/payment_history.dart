import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:FE/api/api.dart';
import 'package:intl/intl.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

void main() => runApp(PaymentHistoryPage());

class PaymentHistoryPage extends StatefulWidget {
  @override
  _PaymentHistoryPageState createState() => new _PaymentHistoryPageState();
}

class _PaymentHistoryPageState extends State<PaymentHistoryPage> {
  List senders = [];
  List recivers = [];

  @override
  void initState() {
    _getPaymentHistory();
    super.initState();
  }

  void _getPaymentHistory() async {
    var res = await CallAPI().getData('payment-history');
    var body = json.decode(res.body);

    setState(() {
      senders = body['sender'];
      recivers = body['reciver'];
    });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context, false)),
            title: Text('Lịch sử thanh toán'),
            bottom: TabBar(
              tabs: <Widget>[
                Tab(text: 'Nhận vào'),
                Tab(text: 'Gửi đi'),
              ],
            ),
          ),
          body: TabBarView(children: <Widget>[
            Container(
                child: recivers.length == 0
                    ? Center(
                        child: Text('Bạn chưa có giao dịch nào!',
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                            )))
                    : Column(
                        children: recivers.map((item) {
                        return Card(
                          child: ListTile(
                            title: Text(
                                'Nhận ${FlutterMoneyFormatter(amount: item['money'].toDouble()).output.withoutFractionDigits}đ từ ${item['sender']['name']}'),
                            subtitle: Text(DateFormat('dd/MM/yyyy')
                                .format(DateTime.parse(item['created_at']))),
                            trailing: Icon(Icons.call_received_rounded),
                            onTap: null,
                          ),
                        );
                      }).toList())),
            Container(
                child: senders.length == 0
                    ? Center(
                        child: Text('Bạn chưa có giao dịch nào!',
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                            )))
                    : Column(
                        children: senders.map((item) {
                        return Card(
                          child: ListTile(
                            title: Text(
                                'Chuyển ${FlutterMoneyFormatter(amount: item['money'].toDouble()).output.withoutFractionDigits}đ cho ${item['reciver']['name']}'),
                            subtitle: Text(DateFormat('dd/MM/yyyy')
                                .format(DateTime.parse(item['created_at']))),
                            trailing: Icon(Icons.call_made_rounded),
                            onTap: null,
                          ),
                        );
                      }).toList())),
          ]),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
