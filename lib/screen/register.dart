import 'dart:convert';

import 'package:FE/api/api.dart';
import 'package:FE/screen/login.dart';
import 'package:flutter/material.dart';

void main() => runApp(RegisterPage());

class RegisterPage extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  bool _isLoading = false;
  bool _success = false;
  bool _showPassword = false;
  bool _showCPassword = false;
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _cpasswordController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showSnackBar(mess, color) {
    final snackBar = new SnackBar(
      content: Text(mess),
      duration: Duration(seconds: 3),
      backgroundColor: color,
      width: 280.0,
      padding: EdgeInsets.symmetric(horizontal: 10), 
      behavior: SnackBarBehavior.floating,
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false ,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back), 
            onPressed: () => Navigator.pop(context, false)
          ),
          title: Text("Đăng Ký"),
        ),
        body: Center(
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: TextField(
                controller: _nameController,
                textCapitalization: TextCapitalization.words,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: "Họ và tên",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder()
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
              child: TextField(
                controller: _emailController,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder()
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
              child: TextField(
                controller: _phoneController,
                keyboardType: TextInputType.number,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: "Số điện thoại",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder()
                ),
              ),
            ),
            Stack(children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: TextField(
                  controller: _passwordController,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    labelText: "Mật khẩu",
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    border: OutlineInputBorder(),
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: this._showPassword ? Colors.blue : Colors.grey,
                      ),
                      onPressed: () {
                        setState(
                            () => this._showPassword = !this._showPassword);
                      },
                    ),
                  ),
                  obscureText: !_showPassword,
                ),
              ),
            ]),
            Stack(children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: TextField(
                  controller: _cpasswordController,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    labelText: "Nhập lại mật khẩu",
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    border: OutlineInputBorder(),
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: this._showCPassword ? Colors.blue : Colors.grey,
                      ),
                      onPressed: () {
                        setState(
                            () => this._showCPassword = !this._showCPassword);
                      },
                    ),
                  ),
                  obscureText: !_showCPassword,
                ),
              ),
            ]),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
              child: TextField(
                controller: _addressController,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: "Địa chỉ liên hệ",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder()
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  color: Colors.greenAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(
                    _isLoading ? 'Đang xử lý ...' :'Đăng ký',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: _isLoading? null : hanldeSignUp,
                ),
              ),
            ),
            _success ? Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  color: Colors.blueAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(
                    'Đăng nhập ngay',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage())),
                ),
              ),
            ) : Text(""),
          ]),
        ),
      ),
    );
  }

  void hanldeSignUp() async {
    this.setState(() {
      _isLoading = true;
    });
    
    var data = {
      'name': _nameController.text,
      'email': _emailController.text,
      'password': _passwordController.text,
      'c_password': _cpasswordController.text,
      'phone': _phoneController.text,
      'address': _addressController.text,
    };

    var res = await CallAPI().postData(data, 'register');
    var body = json.decode(res.body);
   
    this.setState(() {
      _isLoading = false;
    });
    
    if(body['status']){
      //Dang ky thanh cong
      this.setState(() {
        _success = true;
      });
      _showSnackBar('Đăng ký tài khoản thành công!', Colors.greenAccent);
    }
    else{
      //Dang ky that bai
      _showSnackBar(body['message'], Colors.redAccent);
    }
  }
}