import 'dart:convert';

import 'package:FE/api/api.dart';
import 'package:flutter/material.dart';

void main() => runApp(ForgotPasswordPage());

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPasswordPage> {
   bool _isLoading = false;
  TextEditingController _emailController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back), 
            onPressed: () => Navigator.pop(context, false)
          ),
          title: Text("Quên mật khẩu"),
        ),
        body: Center(
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(50, 50, 50, 0),
              child: Text(
                'Vui lòng nhập Email mà bạn đã đăng ký với chúng tôi!',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 18,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
              child: TextField(
                controller: _emailController,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder()
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  color: Colors.blueAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(
                    'Gửi',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: _isLoading ? null :  hanldeSent,
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  void hanldeSent() async {
    // this.setState(() {
    //   _isLoading = true;
    // });

    // var data = {
    //   'email': _emailController.text,
    // };

    // var res = await CallAPI().postData(data, 'forgot');
    // var body = json.decode(res.body);
    // print(body);

    // this.setState(() {
    //   _isLoading = false;
    // });
  }
}