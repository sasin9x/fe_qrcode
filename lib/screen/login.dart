import 'dart:convert';

import 'package:FE/api/api.dart';
import 'package:FE/screen/forgot_password.dart';
import 'package:flutter/material.dart';
import 'package:FE/screen/auth/home.dart';
import 'package:FE/screen/register.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(LoginPage());

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  bool _isLoading = false;
  bool _showPassword = false;
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  var _errorValid = false;
  var _errorEmail = '';
  var _errorPassword = '';

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showSnackBar(mess, color) {
    final snackBar = new SnackBar(
      content: Text(mess),
      duration: Duration(seconds: 3),
      backgroundColor: color,
      width: 280.0,
      padding: EdgeInsets.symmetric(horizontal: 10), 
      behavior: SnackBarBehavior.floating,
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(30, 50, 30, 10),
              child: Container(
                width: 100,
                height: 100,
                child: Icon(
                  Icons.qr_code,
                  color: Colors.blue,
                  size: 100.0,
                  semanticLabel: 'Text to announce in accessibility modes',
                ),
              ),
            ),
            Text(
              "Thanh Toán Điện Tử",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blueAccent,
                fontSize: 35,
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 30, 50, 0),
              child: TextField(
                controller: _emailController,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                  border: OutlineInputBorder(),
                  errorText: _errorValid ? _errorEmail : null,
                ),
              ),
            ),
            Stack(children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: TextField(
                  controller: _passwordController,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    labelText: "Mật khẩu",
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    border: OutlineInputBorder(),
                    errorText: _errorValid ? _errorPassword : null,
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: this._showPassword ? Colors.blue : Colors.grey,
                      ),
                      onPressed: () {
                        setState(
                            () => this._showPassword = !this._showPassword);
                      },
                    ),
                  ),
                  obscureText: !_showPassword,
                ),
              ),
            ]),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 50, 50, 0),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  color: Colors.blueAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(
                    'Đăng nhập',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: _isLoading ? null : hanldeSignIn,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  color: Colors.greenAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Text(
                    'Đăng ký',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => RegisterPage())),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(50, 20, 50, 0),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: FlatButton(
                  child: Text(
                    'Quên mật khẩu ?',
                    style: TextStyle(
                      color: Colors.blue,
                      fontSize: 18,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ForgotPasswordPage())),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  void hanldeSignIn() async {
    this.setState(() {
      _isLoading = true;
    });

    var data = {
      'email': _emailController.text,
      'password': _passwordController.text,
    };

    var res = await CallAPI().postData(data, 'login');
    var body = json.decode(res.body);

    this.setState(() {
      _isLoading = false;
    });

    if (body['status']) {
      //Dang nhap thanh cong
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['data']['token']);

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else {
      //Dang nhap that bai
      _showSnackBar('Đăng nhập không thành công!', Colors.redAccent);
    }
  }
}
