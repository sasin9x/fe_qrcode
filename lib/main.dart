import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:FE/screen/auth/home.dart';
import 'package:FE/screen/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp>{
  var token ;
  
  @override
  void initState() {
    _getToken();
    super.initState();
  }

  void _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    setState(() {
      token = localStorage.getString('token');
    });
  }

  Widget build(BuildContext context){
    return MaterialApp(
      home: (token != null) ? HomePage() : LoginPage(),
      // home: LoginPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
